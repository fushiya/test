// запуск анімації фонових ліній при загрузці сторінки
$(window).on('load', () => {
    $('#loader').css("top", "0");
});


// виклик модульного вікна
$('#btn-buy').click(e => {
    $('#modal').css("top", "50%");
});

// відправка запиту AJAX із форми. Форма змінюється як і при success так і при помилці,
// для того, щоб можна було побачити success без сервера.
// + при помилці в консоль генерується виключення.
$('form').submit(e => {
    e.preventDefault();
    $.ajax({
        url: "http://localhost:3000/post",
        contentType: "application/json",
        method: "POST",
        data: JSON.stringify({
            name: e.target.name.value,
            phone: e.target.phone.value
        }),
        success: res => $('#modal form').css('transition', 'ease 1s').css('margin-left', '-430px'),
        error: err => {
        	$('#modal form').css('transition', 'ease 1s').css('margin-left', '-430px');
        	console.log(err);
        }
    });
});
